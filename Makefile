postgres:
	docker run --name postgres12 -p 5432:5432 -e POSTGRES_USER=root -e POSTGRES_PASSWORD=sanjeev -d postgres:12-alpine
createdb:
	docker exec -it postgres12 createdb --username=root --owner=root sample_bank

dropdb:
	docker exec -it postgres12 dropdb sample_bank

migrateup:
	migrate -path db/migration -database "postgresql://root:sanjeev@localhost:5432/sample_bank?sslmode=disable" -verbose up

migratedown:
	migrate -path db/migration -database "postgresql://root:sanjeev@localhost:5432/sample_bank?sslmode=disable" -verbose down

sqlc:
	docker run --rm -v "$(CURDIR):/src" -w /src kjconroy/sqlc generate

.PHONY: createdb dropdb postgres migrateup migratedown sqlc